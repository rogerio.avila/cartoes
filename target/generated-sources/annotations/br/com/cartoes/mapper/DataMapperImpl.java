package br.com.cartoes.mapper;

import br.com.cartoes.dto.CartaoDto;
import br.com.cartoes.dto.CartaoInvalidadoDto;
import br.com.cartoes.dto.ClienteDto;
import br.com.cartoes.dto.PagtoDto;
import br.com.cartoes.dto.builder.CartaoDtoBuilder;
import br.com.cartoes.dto.builder.CartaoInvalidadoDtoBuilder;
import br.com.cartoes.dto.builder.ClienteDtoBuilder;
import br.com.cartoes.dto.builder.PagtoDtoBuilder;
import br.com.cartoes.entity.Cartao;
import br.com.cartoes.entity.Cliente;
import br.com.cartoes.entity.Pagto;
import br.com.cartoes.entity.builder.CartaoBuilder;
import br.com.cartoes.entity.builder.ClienteBuilder;
import br.com.cartoes.entity.builder.PagtoBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-10T19:59:21-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (JetBrains s.r.o)"
)
@Component
public class DataMapperImpl implements DataMapper {

    @Override
    public Cliente clienteDtoToCliente(ClienteDto clienteDto) {
        if ( clienteDto == null ) {
            return null;
        }

        ClienteBuilder cliente = Cliente.builder();

        cliente.nome( clienteDto.getNome() );

        return cliente.build();
    }

    @Override
    public ClienteDto clienteToClienteDto(Cliente cliente) {
        if ( cliente == null ) {
            return null;
        }

        ClienteDtoBuilder clienteDto = ClienteDto.builder();

        clienteDto.id( cliente.getId() );
        clienteDto.nome( cliente.getNome() );

        return clienteDto.build();
    }

    @Override
    public List<ClienteDto> clienteToClienteDto(List<Cliente> cliente) {
        if ( cliente == null ) {
            return null;
        }

        List<ClienteDto> list = new ArrayList<ClienteDto>( cliente.size() );
        for ( Cliente cliente1 : cliente ) {
            list.add( clienteToClienteDto( cliente1 ) );
        }

        return list;
    }

    @Override
    public Cartao cartaoDtoToCartao(CartaoDto cartaoDto) {
        if ( cartaoDto == null ) {
            return null;
        }

        CartaoBuilder cartao = Cartao.builder();

        cartao.numero( cartaoDto.getNumero() );
        cartao.ativo( cartaoDto.isAtivo() );

        return cartao.build();
    }

    @Override
    public CartaoDto cartaoToCartaoDto(Cartao cartao) {
        if ( cartao == null ) {
            return null;
        }

        CartaoDtoBuilder cartaoDto = CartaoDto.builder();

        cartaoDto.clienteId( cartaoClienteId( cartao ) );
        cartaoDto.id( cartao.getId() );
        cartaoDto.numero( cartao.getNumero() );
        cartaoDto.ativo( cartao.isAtivo() );

        return cartaoDto.build();
    }

    @Override
    public CartaoInvalidadoDto cartaoToCartaoSemEstadoDto(Cartao cartao) {
        if ( cartao == null ) {
            return null;
        }

        CartaoInvalidadoDtoBuilder cartaoInvalidadoDto = CartaoInvalidadoDto.builder();

        cartaoInvalidadoDto.clienteId( cartaoClienteId( cartao ) );
        cartaoInvalidadoDto.id( cartao.getId() );
        cartaoInvalidadoDto.numero( cartao.getNumero() );

        return cartaoInvalidadoDto.build();
    }

    @Override
    public List<CartaoDto> cartaoToCartaoDto(List<Cartao> cartao) {
        if ( cartao == null ) {
            return null;
        }

        List<CartaoDto> list = new ArrayList<CartaoDto>( cartao.size() );
        for ( Cartao cartao1 : cartao ) {
            list.add( cartaoToCartaoDto( cartao1 ) );
        }

        return list;
    }

    @Override
    public Pagto pagtoDtoToPagto(PagtoDto pagtoDto) {
        if ( pagtoDto == null ) {
            return null;
        }

        PagtoBuilder pagto = Pagto.builder();

        pagto.descricao( pagtoDto.getDescricao() );
        pagto.valor( pagtoDto.getValor() );

        return pagto.build();
    }

    @Override
    public PagtoDto pagtoToPagtoDto(Pagto pagto) {
        if ( pagto == null ) {
            return null;
        }

        PagtoDtoBuilder pagtoDto = PagtoDto.builder();

        pagtoDto.cartaoId( pagtoCartaoId( pagto ) );
        pagtoDto.id( pagto.getId() );
        pagtoDto.descricao( pagto.getDescricao() );
        pagtoDto.valor( pagto.getValor() );

        return pagtoDto.build();
    }

    @Override
    public List<PagtoDto> pagtoToPagtoDto(List<Pagto> pagto) {
        if ( pagto == null ) {
            return null;
        }

        List<PagtoDto> list = new ArrayList<PagtoDto>( pagto.size() );
        for ( Pagto pagto1 : pagto ) {
            list.add( pagtoToPagtoDto( pagto1 ) );
        }

        return list;
    }

    private Long cartaoClienteId(Cartao cartao) {
        if ( cartao == null ) {
            return null;
        }
        Cliente cliente = cartao.getCliente();
        if ( cliente == null ) {
            return null;
        }
        Long id = cliente.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long pagtoCartaoId(Pagto pagto) {
        if ( pagto == null ) {
            return null;
        }
        Cartao cartao = pagto.getCartao();
        if ( cartao == null ) {
            return null;
        }
        Long id = cartao.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}

package br.com.cartoes.service;

import br.com.cartoes.entity.Cartao;
import br.com.cartoes.entity.Pagto;
import br.com.cartoes.exception.CartaoNotFoundException;
import br.com.cartoes.repository.CartaoRepository;
import br.com.cartoes.repository.PagtoRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class PagtoService {

    private final PagtoRepository pagtoRepository;
    private final CartaoRepository cartaoRepository;


    public PagtoService(PagtoRepository pagtoRepository, CartaoRepository cartaoRepository) {
        this.pagtoRepository = pagtoRepository;
        this.cartaoRepository = cartaoRepository;
    }

    public Pagto save(Long cartaoId, Pagto pagto) throws CartaoNotFoundException {
        Cartao cartao = getCartaoImpl(cartaoId);
        pagto.setCartao(cartao);
        return pagtoRepository.save(pagto);
    }


    public List<Pagto> getPagtosPorCartao(Long cartaoId) throws CartaoNotFoundException {
        Cartao cartao = getCartaoImpl(cartaoId);
        return pagtoRepository.findAllByCartao(cartao);
    }

    private Cartao getCartaoImpl(Long cartaoId) throws CartaoNotFoundException {
        Optional<Cartao> cartao = cartaoRepository.findById(cartaoId);
        if (!cartao.isPresent()) {
            throw new CartaoNotFoundException("O cartão informado não foi encontrado.");
        }
        return cartao.get();
    }
}

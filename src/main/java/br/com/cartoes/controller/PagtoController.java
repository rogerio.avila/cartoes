package br.com.cartoes.controller;

import br.com.cartoes.dto.PagtoDto;
import br.com.cartoes.entity.Pagto;
import br.com.cartoes.mapper.DataMapper;
import br.com.cartoes.service.PagtoService;
import br.com.cartoes.exception.CartaoNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.List;

@RestController
@Validated
public class PagtoController {

    private final PagtoService pagtoService;



    public PagtoController(PagtoService pagtoService) {
        this.pagtoService = pagtoService;
    }

    @PostMapping("/pagamento")
    public ResponseEntity saveCartao(@RequestBody @Valid PagtoDto pagtoDto) throws CartaoNotFoundException {
        Pagto pagto = DataMapper.INSTANCE.pagtoDtoToPagto(pagtoDto);
        Pagto savedPagto = pagtoService.save(pagtoDto.getCartaoId(), pagto);
        return ResponseEntity.created(URI.create("")).body(DataMapper.INSTANCE.pagtoToPagtoDto(savedPagto));
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public ResponseEntity getPagtos(
            @Valid
            @Min(value = 1, message = "O id do cartão deve ser maior que zero.")
            @PathVariable("id_cartao") Long cartaoId) throws CartaoNotFoundException {
        List<Pagto> pagtosPorCartao = pagtoService.getPagtosPorCartao(cartaoId);
        return ResponseEntity.created(URI.create("")).body(DataMapper.INSTANCE.pagtoToPagtoDto(pagtosPorCartao));
    }
}

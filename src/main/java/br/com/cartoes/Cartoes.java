package br.com.cartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cartoes {

    public static void main(String[] args) {
        SpringApplication.run(Cartoes.class, args);
    }

}

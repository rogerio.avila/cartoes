package br.com.cartoes.entity.builder;

import br.com.cartoes.entity.Cartao;
import br.com.cartoes.entity.Cliente;
import br.com.cartoes.entity.Pagto;

import java.util.ArrayList;
import java.util.List;

public final class CartaoBuilder {


    private String numero;
    private boolean ativo = Boolean.FALSE;
    private Cliente cliente;
    private List<Pagto> pagtos = new ArrayList<>();

    private CartaoBuilder() {
    }

    public static CartaoBuilder aCartao() {
        return new CartaoBuilder();
    }

    public CartaoBuilder numero(String numero) {
        this.numero = numero;
        return this;
    }

    public CartaoBuilder ativo(boolean ativo) {
        this.ativo = ativo;
        return this;
    }

    public CartaoBuilder cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public CartaoBuilder pagtos(List<Pagto> pagtos) {
        this.pagtos = pagtos;
        return this;
    }

    public Cartao build() {
        return new Cartao(numero, ativo, cliente, pagtos);
    }
}

package br.com.cartoes.entity.builder;

import br.com.cartoes.entity.Pagto;
import br.com.cartoes.entity.Cartao;

public final class PagtoBuilder {
    private String descricao;
    private double valor;
    private Cartao cartao;

    private PagtoBuilder() {
    }

    public static PagtoBuilder aPagto() {
        return new PagtoBuilder();
    }

    public PagtoBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public PagtoBuilder valor(double valor) {
        this.valor = valor;
        return this;
    }

    public PagtoBuilder cartao(Cartao cartao) {
        this.cartao = cartao;
        return this;
    }

    public Pagto build() {
        return new Pagto(descricao, valor, cartao);
    }
}

package br.com.cartoes.entity;

import br.com.cartoes.entity.builder.PagtoBuilder;

import javax.persistence.*;

@Entity
@Table(name = "pagamento")
public class Pagto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "valor")
    private double valor;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cartao_id")
    private Cartao cartao;

    public Pagto() {
    }

    public Pagto(String descricao, double valor, Cartao cartao) {
        this.descricao = descricao;
        this.valor = valor;
        this.cartao = cartao;
    }

    public static PagtoBuilder builder() {
        return PagtoBuilder.aPagto();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getValor() {
        return valor;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}

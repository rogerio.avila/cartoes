package br.com.cartoes.repository;

import br.com.cartoes.entity.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numeroCartao);
}

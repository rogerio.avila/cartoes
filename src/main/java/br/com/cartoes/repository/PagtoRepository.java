package br.com.cartoes.repository;

import br.com.cartoes.entity.Cartao;
import br.com.cartoes.entity.Pagto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagtoRepository extends CrudRepository<Pagto, Long> {
    List<Pagto> findAllByCartao(Cartao cartao);
}

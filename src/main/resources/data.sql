DROP TABLE IF EXISTS cliente CASCADE;

CREATE TABLE cliente (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL
);


DROP TABLE IF EXISTS cartao CASCADE;

CREATE TABLE cartao (
    id INT AUTO_INCREMENT PRIMARY KEY,
    numero VARCHAR(12) NOT NULL UNIQUE,
    ativo BOOL NOT NULL DEFAULT FALSE,
    cliente_id INT NOT NULL,
    foreign key (cliente_id) references cliente(id)
);


DROP TABLE IF EXISTS pagto;

CREATE TABLE pagto (
     id INT AUTO_INCREMENT PRIMARY KEY,
     descricao VARCHAR(100) NOT NULL,
     valor DOUBLE NOT NULL,
     cartao_id INT NOT NULL,
     foreign key (cartao_id) references cartao(id)
);